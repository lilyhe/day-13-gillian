const promise = new Promise((resolve, reject) => {
      if (Math.random() < 0.5) {
        resolve('success');
      } else {
        reject('error');
      }
    });
   
  promise.then(result => {
    console.log(result);
  }).catch(error => {
    console.log(error);
  });