# day13-Gillian

## Objective:
Through the code review, I fixed the bug in yesterday's assignment.
We learned Router in react. Router keeps the Ul consistent with the URL. When the route changed, it will render different components instead of constantly switching between different pages. 
Use axios and Mock API for back-end data interaction.
Learn to use Ant Design to make the UI more beautiful.

## Reflective:
Fullfilled

## Interpretive:
Ant Design is easy to use, which makes it easier for me to implement style effect.

## Decisional:
Take the time to clarify the process of connecting the front and back ends. I often write code and forget why I'm writing it.













