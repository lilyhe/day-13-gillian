import { useDispatch } from 'react-redux';
import { loadTodos } from '../apis/todo';
import { initTodos } from '../components/todoSlice';

export const useTodo = () => {
    const dispatch = useDispatch();

    const reloadTodos = () => {
        loadTodos().then((response) => {            
            dispatch(initTodos(response.data))
    });
}
    return {
        reloadTodos,
    }
}