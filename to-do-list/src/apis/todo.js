import request from "./client";

export const loadTodos = () => {
    return request.get("/todoList")
};

export const updateTodoList = (id, done) => {
    return request.put(`/todoList/${id}`, {
        done,
    })
}

export const addTodoList = (todoList) => {
    return request.post(`/todoList`, todoList)
}

export const deleteTodoList = (id) => {
    return request.delete(`/todoList/${id}`)
}

export const updateTodoText = (id, text) => {
    return request.put(`/todoList/${id}`, {
        text,
    });
  };
  

