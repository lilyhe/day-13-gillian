import { message } from "antd";
import axios from "axios";

const request = axios.create({
    baseURL: "https://64c0c5520d8e251fd1128864.mockapi.io/"
});

request.interceptors.response.use(
    (response) => response,
    (error) => {
        console.log(error.response.data);
        const msg = error.response.data?.msg;
        if (msg) {
            message.error(msg);
        }
        return Promise.reject(error);
    }
);

export default request;