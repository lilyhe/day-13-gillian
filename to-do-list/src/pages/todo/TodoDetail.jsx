import { useParams } from "react-router-dom";
import { useSelector } from 'react-redux';
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
const TodoDetail = () => {
    const params = useParams();

    const { id } = params;
    const navigate = useNavigate();

    const todoList = useSelector(state => state.todo.todoList.find(item => item.id === id));

    useEffect(() => {
        if (!todoList) {
            navigate('/404')
        }}, [todoList]
    );

    return (
        <div>
            <h1>Detail</h1>
            {               
                todoList && 
                (<div><p>{todoList.text}</p></div>)
            }
            
        </div>
    )

}

export default TodoDetail;