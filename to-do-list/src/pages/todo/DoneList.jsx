import { useSelector } from "react-redux";
import { TodoGroup } from "../../components/TodoGroup";
const DoneList = () => {
    const todoList = useSelector(state => state.todo.todoList);

    const doneTodos = todoList.filter(todoList => todoList.done);

    return (
        <div>
            <h1>Done List</h1>
            <TodoGroup todoList={doneTodos} canEdit={false}></TodoGroup>
        </div>
    );
}
export default DoneList;