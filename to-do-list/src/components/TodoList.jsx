import { useSelector } from "react-redux";
import { TodoGroup } from "./TodoGroup";
import { TodoItemGenerator } from "./ToDoItemGenerator";
import { useEffect } from "react";
import { addTodoList } from "../apis/todo";
import { useTodo } from "../hooks/useTodo";

export const TodoList = () => {
    const todoList = useSelector(state => state.todo.todoList);
    const { reloadTodos } = useTodo();

    useEffect(() => {
        reloadTodos();
    }, []);

    const handleUpdateTodoList = async (newTodo) => {
        await addTodoList(newTodo);
        reloadTodos();
    }
    return (
        <div>
            <h1>Todo List</h1>
            <TodoGroup todoList={todoList} canEdit={true}></TodoGroup>
            <TodoItemGenerator onChange={handleUpdateTodoList}></TodoItemGenerator>
        </div>
    )
}
