import { Outlet } from "react-router-dom"
import { NavLink } from "react-router-dom";
import '../style/layout.css';

const Layout = () => {
    return (
        <div>
            <div className="header">
                <NavLink className="link" to="/">HOME</NavLink>
                <NavLink className="link" to="/done">DONE</NavLink>
                <NavLink className="link" to="/about">ABOUT</NavLink>
            </div>
            <Outlet></Outlet>
        </div>
    )
}

export default Layout;