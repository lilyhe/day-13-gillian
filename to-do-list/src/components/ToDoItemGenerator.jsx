import { Button } from "antd";
import { useState } from "react";

export const TodoItemGenerator = (props) => {
    const [content, setContent] = useState('');

    const handleChangeInput = (e) => {
        setContent(e.target.value);
    }

    const handleChange = () => {
        props.onChange({
            id: new Date().toString(),
            text: content,
            done: false,
        });
        setContent('');
    }

    const handleKeyUp = (event) => {
        if(event.keyCode === 13) {
            handleChange()
        }

    }

    return (
        <div className="add-item">
            <input type="text" value={content} onChange={handleChangeInput} onKeyUp={handleKeyUp}></input>
            <Button type="primary" className="add" onClick={handleChange} size="small">add</Button>
        </div>
    )
}
