import '../style/todoItem.css';
import { updateTodoList, updateTodoText } from '../apis/todo';
import { deleteTodoList } from '../apis/todo';
import { useNavigate } from 'react-router-dom';
import { useTodo } from "../hooks/useTodo";
import { Button, Modal, Input } from 'antd';
import React, { useState } from 'react';

export const TodoItem = (props) => {
     const { reloadTodos } = useTodo();
     const navigate = useNavigate();
     const [isModalOpen, setIsModalOpen] = useState(false);
     const [todoText, setTodoText] = useState(props.value.text);
     
     const handleClick = async () => {
        if (props.canEdit) {
            await updateTodoList(props.value.id, !props.value.done)
            reloadTodos();
        }
        else {
            navigate(`/todo/${props.value.id}`);
        }
     }

     const handleDeleteClick = async () => {
        try {
            await deleteTodoList(props.value.id)
            reloadTodos();
        } catch{}
     }

     const showModal = () => {
       setIsModalOpen(true);
     };

     const handleOk = async () => {
        const { id } = props.value;
        await updateTodoText(id, todoText);
        reloadTodos();
        setIsModalOpen(false);
     };

     const handleCancel = () => {
       setIsModalOpen(false);
     };

    return (
        <div className='todo-item'>
            <span className={props.value.done ? 'finish' : 'unfinish'}  onClick={ handleClick } >{props.value.text}</span>
            <Button type="primary" className="edit" onClick={showModal} size="small">edit</Button>
            <Button type="primary" className="delete" onClick= { handleDeleteClick } size="small">x</Button>

            <Modal title="Edit Todo" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                <Input value={todoText} onChange={event => setTodoText(event.target.value)} placeholder="todo item" />
            </Modal>
        </div>
    )
}

