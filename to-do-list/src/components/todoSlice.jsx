import { createSlice } from '@reduxjs/toolkit';

const todoSlice = createSlice({
    name: "todo",
    initialState: {
        todoList: [],
    },
    reducers: {
        initTodos: (state, action) => {
            state.todoList = action.payload;
        },

        addTodoList: (state, action) => {
            state.todoList = [...state.todoList, action.payload];
        },

        updateTodoList: (state, action) => {
            state.todoList.map(item => {
                if (item.id === action.payload) {  
                    item.done = !item.done
                }
                return state.todoList;
                }
            );
        },

        deleteTodoList: (state, action) => {
            state.todoList = state.todoList.filter(item => item.id !== action.payload);
        },
    },

})

export const { addTodoList } = todoSlice.actions;
export const { updateTodoList } = todoSlice.actions;
export const { deleteTodoList } = todoSlice.actions;
export const { initTodos } = todoSlice.actions;
export default todoSlice.reducer;